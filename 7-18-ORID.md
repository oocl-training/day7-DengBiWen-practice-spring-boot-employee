## ORID

### O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
1. Http
2. RESTful API
3. Pair Programming
4. SpringBoot
```
### R (Reflective)
Please use one word to express your feelings about today's class.
```
full
```
### I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
RESTful focuses on resource manipulation, i.e. state transfer.
Use mainly get, post, put, delete to transfer state.
```
### D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
Use RESTful API to design request interface. It is more expressive than traditional style.
```