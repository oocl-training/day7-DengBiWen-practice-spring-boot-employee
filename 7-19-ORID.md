## ORID

### O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
1. Integration Test
2. TDD in SpringBoot
3. Global exception handler advice
4. Some annotation
5. Two Api given and verify of Mockito impress me, use them first and feel strange.
```
### R (Reflective)

Please use one word to express your feelings about today's class.
```
tired
```
### I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
Integration Test and TDD in SpringBoot is more convenient than request application such as postman.
In SpringBoot, I can implement the automating test with mock.
```
### D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
Use them to automate testing.
I will use it to replace postman.
```