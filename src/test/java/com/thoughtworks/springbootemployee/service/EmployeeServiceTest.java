package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.InactiveEmployeeException;
import com.thoughtworks.springbootemployee.exception.SalaryNotMatchAgeException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

	@Test
	void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
		EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
		Employee employee = new Employee(1L, "jj", 7, Gender.FEMALE, 60000.0, 1L);
		assertThrows(AgeIsInvalidException.class, () -> employeeService.addEmployee(employee));
	}

	@Test
	void should_not_create_successfully_when_create_employee_given_age_is_over_30_but_salary_is_below_20000() {
		EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
		Employee employee = new Employee(1L, "jj", 41, Gender.FEMALE, 10000.0, 1L);
		assertThrows(SalaryNotMatchAgeException.class, () -> employeeService.addEmployee(employee));
	}

	@Test
	void should_return_active_employee_when_addEmployee_given_valid_employee() {
		// given
		EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
		EmployeeService employeeService = new EmployeeService(employeeRepository);
		Employee employee = new Employee(null, "Baldwin", 41, Gender.FEMALE, 30000.0, 1L);
		// when
		employeeService.addEmployee(employee);
		// then
		verify(employeeRepository).insert(argThat(employeeVerified -> {
			assertTrue(employeeVerified.getActive());
			return true;
		}));
	}

	@Test
	void should_return_employee_without_active_when_deleteEmployee_given_id_of_active_employee() {
		// given
		EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
		EmployeeService employeeService = new EmployeeService(employeeRepository);
		Employee employee = new Employee(null, "Baldwin", 41, Gender.FEMALE, 30000.0, 1L);
		//employeeService.addEmployee(employee);
		// use mock repo to assuming data in repo, so needn't add really
		given(employeeRepository.selectById(1L)).willReturn(employee);
		// when
		employeeService.deleteEmployee(1L);
		// then
		verify(employeeRepository, times(1)).update(employee);
	}

	@Test
	void should_throw_inactive_exception_when_updateEmployee_given_inactive_employee() {
		// given
		EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
		EmployeeService employeeService = new EmployeeService(employeeRepository);
		Employee employee = new Employee(1L, "Li Ming", 41, Gender.FEMALE, 30000.0, 1L);
		employee.setActive(false);
		given(employeeRepository.selectById(1L)).willReturn(employee);
		// when

		// then
		assertThrows(InactiveEmployeeException.class,
				() -> employeeService.updateEmployeeAgeAndSalary(employee.getId(), employee));
	}

}