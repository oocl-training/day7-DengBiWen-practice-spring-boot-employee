package com.thoughtworks.springbootemployee.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc client;

    @BeforeEach
    void cleanEmployeeData() {
        companyRepository.clearAll();
    }

    @Test
    void should_return_companies_when_obtainCompanies()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        companyRepository.insert(company);
        for (int i = 1; i <= 15; i++) {
            companyRepository.insert(new Company((long) i, "OOCL" + i));
        }
        // then
        client.perform(MockMvcRequestBuilders.get("/companies")).andExpect(status().isOk())
              .andExpect(jsonPath("$", hasSize(16))).andExpect(jsonPath("$[0].id").value(company.getId()))
              .andExpect(jsonPath("$[0].name").value(company.getName()));
    }


    @Test
    void should_return_created_company_when_perform_addCompany_given_company()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        // then
        client.perform(MockMvcRequestBuilders.post("/companies").contentType(MediaType.APPLICATION_JSON)
                                             .content(companyJson)).andExpect(status().isCreated())
              .andExpect(jsonPath("$.name").value(company.getName()))
              .andExpect(jsonPath("$.id").value(company.getId()));
    }

    @Test
    void should_return_company_when_obtainCompanyById_given_id()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        companyRepository.insert(company);
        // then
        client.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId()))
              .andExpect(status().isOk()).andExpect(jsonPath("$.name").value(company.getName()))
              .andExpect(jsonPath("$.id").value(company.getId()));
    }

    @Test
    void should_updated_company_when_updateCompany_given_company()
    throws Exception {
        Company company = new Company(1L, "OOCL");
        Company companyUpdated = new Company(1L, "CargoSmart");
        String companyUpdatedJson = new ObjectMapper().writeValueAsString(companyUpdated);
        companyRepository.insert(company);
        // then
        client.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId())
                                             .contentType(MediaType.APPLICATION_JSON)
                                             .content(companyUpdatedJson)).andExpect(status().isOk())
              .andExpect(jsonPath("$.id").value(company.getId()))
              .andExpect(jsonPath("$.name").value(companyUpdated.getName()));
    }


    @Test
    void should_delete_company_when_deleteCompany_given_id()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        companyRepository.insert(company);
        // then
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
              .andExpect(status().isNoContent());
    }

    @Test
    void should_return_a_page_of_companies_when_getOnePageOfCompanies_given_page_and_size()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        companyRepository.insert(company);
        for (int i = 1; i <= 15; i++) {
            companyRepository.insert(new Company((long) i, "OOCL" + i));
        }
        // then
        client.perform(MockMvcRequestBuilders.get("/companies").param("page", "1").param("size", "3"))
              .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(3)))
              .andExpect(jsonPath("$[0].id").value(company.getId()))
              .andExpect(jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_return_employees_with_companyId_is_given_companyId_when_obtainEmployeesOfCompany_given_companyId()
    throws Exception {
        // given
        Company company = new Company(1L, "OOCL");
        companyRepository.insert(company);
        for (int i = 1; i <= 15; i++) {
            companyRepository.insert(new Company(null, "OOCL" + i));
        }

        Employee aimy = new Employee(null, "aimy", 33, Gender.FEMALE, 60000.0, 1L);
        Employee EmployeeCreated = employeeRepository.insert(aimy);
        for (int i = 1; i <= 10; i++) {
            employeeRepository.insert(new Employee(null, "aimy" + i, 33, Gender.FEMALE, 60000.0, 2L));
            employeeRepository.insert(new Employee(null, "aimy" + i, 33, Gender.FEMALE, 60000.0, 1L));
        }

        // then
        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees",company.getId()))
              .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(11)))
              .andExpect(jsonPath("$[0].id").value(EmployeeCreated.getId()))
              .andExpect(jsonPath("$[0].name").value(EmployeeCreated.getName()))
              .andExpect(jsonPath("$[0].age").value(EmployeeCreated.getAge()))
              .andExpect(jsonPath("$[0].gender").value("FEMALE"))
              .andExpect(jsonPath("$[0].salary").value(EmployeeCreated.getSalary()))
              .andExpect(jsonPath("$[0].companyId").value(EmployeeCreated.getCompanyId()));
    }

}
