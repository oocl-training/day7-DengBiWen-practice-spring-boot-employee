package com.thoughtworks.springbootemployee.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private MockMvc client;

	@BeforeEach
	void cleanEmployeeData() {
		employeeRepository.clearAll();
	}

	@Test
	void should_return_employees_when_getEmployees_given_employees() throws Exception {
		// given
		Employee employee = new Employee(1L, "job", 17, Gender.FEMALE, 10000.0, 1L);
		employeeRepository.insert(employee);
		client.perform(MockMvcRequestBuilders.get("/employees"))
		      .andExpect(status().isOk());
	}

	@Test
	void should_return_employees_filter_by_gender_when_perform_findEmployeeByGender_given_gender() throws Exception {
		Employee aimy = new Employee(2L, "aimy", 33, Gender.FEMALE, 60000.0, 2L);
		employeeRepository.insert(aimy);
		client.perform(MockMvcRequestBuilders.get("/employees")
		                                     .param("gender", "FEMALE"))
		      .andExpect(status().isOk());
	}

	@Test
	void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
		// given
		String johnJson = "    {\n" + "        \"id\": 1,\n" + "        \"name\": \"baldwin\",\n" + "        \"age\": 23,\n" + "        \"gender\": \"FEMALE\",\n" + "        \"salary\": 10000.0,\n" + "        \"companyId\": 1\n" + "    }";
		// when
		client.perform(MockMvcRequestBuilders.post("/employees")
		                                     .contentType(MediaType.APPLICATION_JSON)
		                                     .content(johnJson))
		      .andExpect(status().isCreated())
		      .andExpect(jsonPath("$.name").value("baldwin"))
		      .andExpect(jsonPath("$.age").value(23))
		      .andExpect(jsonPath("$.gender").value("FEMALE"))
		      .andExpect(jsonPath("$.salary").value(10000.0))
		      .andExpect(jsonPath("$.companyId").value(1));
	}

	@Test
	void should_return_employee_when_getEmployeeById_given_employeeId() throws Exception {
		Employee john = new Employee(null, "John", 32, Gender.MALE, 5000.0, 2L);
		employeeRepository.insert(john);
		client.perform(MockMvcRequestBuilders.get("/employees/1"))
		      .andExpect(status().isOk())
		      .andExpect(jsonPath("$.id").value(john.getId()))
		      .andExpect(jsonPath("$.name").value(john.getName()))
		      .andExpect(jsonPath("$.age").value(john.getAge()))
		      .andExpect(jsonPath("$.salary").value(john.getSalary()));
	}

	@Test
	void should_updated_employee_when_updateEmployee_given_employee() throws Exception {
		Employee johnSmith = new Employee(null, "John", 32, Gender.MALE, 115000.0, 1L);
		johnSmith.setActive(true);
		Employee johnSmithUpdate = new Employee(null, "John", 36, Gender.MALE, 119000.0, 1L);
		String johnSmithUpdateJson = new ObjectMapper().writeValueAsString(johnSmithUpdate);
		employeeRepository.insert(johnSmith);
		client.perform(MockMvcRequestBuilders.put("/employees/{id}",johnSmith.getId())
		                                     .contentType(MediaType.APPLICATION_JSON)
		                                     .content(johnSmithUpdateJson))
		      .andExpect(status().isOk())
		      .andExpect(jsonPath("$.id").value(johnSmith.getId()))
		      .andExpect(jsonPath("$.name").value(johnSmithUpdate.getName()))
		      .andExpect(jsonPath("$.age").value(johnSmithUpdate.getAge()))
		      .andExpect(jsonPath("$.salary").value(johnSmithUpdate.getSalary()));
	}


	@Test
	void should_delete_employee_when_deleteEmployee_given_id() throws Exception {
		// given
		Employee aimy = new Employee(2L, "aimy", 33, Gender.FEMALE, 60000.0, 2L);
		employeeRepository.insert(aimy);
		// then
		client.perform(MockMvcRequestBuilders.delete("/employees/{id}", 1))
		      .andExpect(status().isNoContent());
	}

	@Test
	void should_return_a_page_of_employees_when_getOnePageOfEmployees_given_page_and_size() throws Exception {
		// given
		Employee aimy = new Employee(1L, "aimy", 33, Gender.FEMALE, 60000.0, 2L);
		employeeRepository.insert(aimy);
		for (int i = 2; i < 10; i++) {
			Employee tmpEmployee = new Employee((long) i, "aimy" + i, 33, Gender.FEMALE, 60000.0, 2L);
			employeeRepository.insert(tmpEmployee);
		}

		// then
		client.perform(MockMvcRequestBuilders.get("/employees")
		                                     .param("size", "3")
		                                     .param("page", "1"))
		      .andExpect(status().isOk())
		      .andExpect(jsonPath("$", hasSize(3)))
		      .andExpect(jsonPath("$[0].id").value(1))
		      .andExpect(jsonPath("$[0].name").value("aimy"))
		      .andExpect(jsonPath("$[0].age").value(33))
		      .andExpect(jsonPath("$[0].gender").value("FEMALE"))
		      .andExpect(jsonPath("$[0].salary").value(60000.0))
		      .andExpect(jsonPath("$[0].companyId").value(2L));
	}

}
