package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.ResourceNotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
	private final List<Employee> employees = new ArrayList<>();
	private final AtomicLong atomicId = new AtomicLong(0);


	public void clearAll() {
		this.employees.clear();
		atomicId.set(0);
	}

	public Employee insert(Employee employee) {
		employee.setId(atomicId.incrementAndGet());
		this.employees.add(employee);
		return employee;
	}

	public Object getEmployeesNum() {
		return this.employees.size();
	}

	public List<Employee> selectList() {
		return this.employees;
	}

	public Employee selectById(Long id) {
		return employees.stream()
		                .filter(employee -> Objects.equals(employee.getId(), id))
		                .findFirst()
		                .orElse(null);
	}

	public List<Employee> selectByGender(Gender gender) {
		return employees.stream().
		                filter(employee -> Objects.equals(employee.getGender(), gender))
		                .collect(Collectors.toList());
	}

	public Employee update(Employee employee) {
		Employee employeeUpdated = selectById(employee.getId());
		employeeUpdated.setAge(employee.getAge());
		employeeUpdated.setSalary(employee.getSalary());
		employeeUpdated.setActive(employee.getActive());
		employeeUpdated.setGender(employee.getGender());
		employeeUpdated.setCompanyId(employee.getCompanyId());
		employeeUpdated.setName(employee.getName());
		return employee;
	}

	public void deleteById(Long id) {
		Employee employeeDeleted = selectById(id);
		if (employeeDeleted == null) {
			throw new ResourceNotFoundException();
		}
		employees.remove(employeeDeleted);
	}

	public List<Employee> selectList(int page, int size) {
		return employees.stream()
		                .skip((long) (page - 1) * size)
		                .limit(size)
		                .collect(Collectors.toList());
	}

	public List<Employee> selectByCompanyId(Long companyId) {
		return employees
				.stream()
				.filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
				.collect(Collectors.toList());
	}

}
