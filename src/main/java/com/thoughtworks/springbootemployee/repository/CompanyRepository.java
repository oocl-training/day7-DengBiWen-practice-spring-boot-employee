package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.exception.ResourceNotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
	private final List<Company> companies = new ArrayList<>();
	private final AtomicLong atomicId = new AtomicLong(0);


	public void insert(Company company) {
		company.setId(atomicId.incrementAndGet());
		this.companies.add(company);
	}

	public List<Company> select() {
		return this.companies;
	}

	public Company selectById(Long id) {
		return companies.stream()
		                .filter(company -> Objects.equals(company.getId(), id))
		                .findFirst()
		                .orElseThrow(ResourceNotFoundException::new);
	}

	public List<Company> selectPage(Integer page, Integer size) {
		return companies.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
	}

	public Company updateById(Long id, Company company) {
		Company companyUpdated = companies.stream().filter((companyItem -> Objects.equals(companyItem.getId(), id))).findFirst().orElseThrow(ResourceNotFoundException::new);
		companyUpdated.setName(company.getName());
		return companyUpdated;
	}

	public void deleteById(Long id) {
		companies.removeIf(company -> Objects.equals(company.getId(), id));
	}

    public void clearAll() {
		this.companies.clear();
		this.atomicId.set(0);
    }
}
