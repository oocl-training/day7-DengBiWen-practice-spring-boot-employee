package com.thoughtworks.springbootemployee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandlerAdvice {
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Exception> handleResourceNotFoundException(Exception e) {
		e.printStackTrace();
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
		                     .body(e);
	}

	@ExceptionHandler(AgeIsInvalidException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ResponseEntity<Exception> handleAgeIsInvalidException(Exception e) {
		e.printStackTrace();
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
		                     .body(e);
	}

	@ExceptionHandler(SalaryNotMatchAgeException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ResponseEntity<Exception> handleSalaryNotMatchAgeException(Exception e) {
		e.printStackTrace();
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
		                     .body(e);
	}


	@ExceptionHandler(InactiveEmployeeException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ResponseEntity<Exception> handleInactiveEmployeeException(Exception e) {
		e.printStackTrace();
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
		                     .body(e);
	}

}
