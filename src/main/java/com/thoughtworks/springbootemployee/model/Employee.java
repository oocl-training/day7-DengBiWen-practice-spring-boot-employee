package com.thoughtworks.springbootemployee.model;

import com.thoughtworks.springbootemployee.common.Gender;

import java.util.Objects;


public class Employee {

	private Long id;
	private String name;
	private Integer age;
	private Gender gender;
	private Double salary;
	private Long companyId;

	private Boolean isActive;

	public Boolean getActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Integer getAge() {
		return age;
	}

	public Gender getGender() {
		return gender;
	}

	public Double getSalary() {
		return salary;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Employee(Long id, String name, Integer age, Gender gender, Double salary, Long companyId) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.salary = salary;
		this.companyId = Objects.requireNonNullElse(companyId, 1L);
	}

	public boolean isAgeValid() {
		return this.age >= 18 && this.age <= 65;
	}

	public boolean isSalaryMatchAge() {
		if (age <= 30) {
			return true;
		} else return salary >= 20000;
	}


}
