package com.thoughtworks.springbootemployee.common;


public enum Gender {
    FEMALE,
    MALE;
}
