package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Resource
    private EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee queryEmployeeById(@PathVariable Long id) {
        return employeeService.queryEmployeeById(id);
    }


    @GetMapping(params = {"gender"})
    public List<Employee> queryEmployeeByGender(@RequestParam Gender gender) {
        return employeeService.queryEmployeeByGender(gender);
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeAndSalary(@PathVariable Long id, @RequestBody Employee employeeUpdated) {
        return employeeService.updateEmployeeAgeAndSalary(id, employeeUpdated);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getOnePageOfEmployees(@RequestParam int page, int size) {
        return employeeService.getOnePageOfEmployees(page, size);
    }

}
