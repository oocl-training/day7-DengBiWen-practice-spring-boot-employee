package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService companyService;
    private final EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return companyService.addCompany(company);
    }

    @GetMapping
    public List<Company> obtainCompanies() {
        return companyService.obtainCompanies();
    }

    @GetMapping("/{id}")
    public Company obtainCompanyById(@PathVariable Long id) {
        return companyService.obtainCompanyById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> obtainPageOfCompanies(@RequestParam Integer page, Integer size) {
        return companyService.obtainPageOfCompanies(page, size);
    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id, @RequestBody Company company) {
        return companyService.updateCompanyName(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.deleteCompany(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> obtainEmployeesOfCompany(@PathVariable("id") Long companyId) {
        return employeeService.obtainEmployeesOfCompany(companyId);
    }
}
