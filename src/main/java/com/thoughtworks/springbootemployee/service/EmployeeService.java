package com.thoughtworks.springbootemployee.service;


import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.InactiveEmployeeException;
import com.thoughtworks.springbootemployee.exception.ResourceNotFoundException;
import com.thoughtworks.springbootemployee.exception.SalaryNotMatchAgeException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
	private final EmployeeRepository employeeRepository;

	public EmployeeService(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public Employee addEmployee(Employee employee) {
		if (!employee.isAgeValid()) {
			throw new AgeIsInvalidException();
		}
		if (!employee.isSalaryMatchAge()) {
			throw new SalaryNotMatchAgeException();
		}
		employee.setActive(true);
		return employeeRepository.insert(employee);
	}

	public List<Employee> getEmployees() {
		return employeeRepository.selectList();
	}

	public Employee queryEmployeeById(Long id) {
		return employeeRepository.selectById(id);
	}

	public List<Employee> queryEmployeeByGender(Gender gender) {
		return employeeRepository.selectByGender(gender);
	}

	public Employee updateEmployeeAgeAndSalary(Long id, Employee employee) {
		Employee employeeUpdated = employeeRepository.selectById(id);
		if (employeeUpdated == null) {
			throw new ResourceNotFoundException();
		}
		if (!employeeUpdated.getActive()) {
			throw new InactiveEmployeeException();
		}
		employeeUpdated.setAge(employee.getAge());
		employeeUpdated.setSalary(employee.getSalary());
		return employeeRepository.update(employeeUpdated);
	}

	public void deleteEmployee(Long id) {
		Employee employeeDeleted = employeeRepository.selectById(id);
		if (employeeDeleted == null) {
			throw new ResourceNotFoundException();
		}
		employeeDeleted.setActive(false);
		employeeRepository.update(employeeDeleted);
	}


	public List<Employee> getOnePageOfEmployees(int page, int size) {
		return employeeRepository.selectList(page, size);
	}

	public List<Employee> obtainEmployeesOfCompany(Long companyId) {
		return employeeRepository.selectByCompanyId(companyId);
	}
}
