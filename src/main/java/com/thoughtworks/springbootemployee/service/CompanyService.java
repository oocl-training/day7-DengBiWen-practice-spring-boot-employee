package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CompanyService {
	@Autowired
	private CompanyRepository companyRepository;


	public Company addCompany(Company company) {
		companyRepository.insert(company);
		return company;
	}

	public List<Company> obtainCompanies() {
		return companyRepository.select();
	}

	public Company obtainCompanyById(Long id) {
		return companyRepository.selectById(id);
	}

	public List<Company> obtainPageOfCompanies(Integer page, Integer size) {
		return companyRepository.selectPage(page,size);
	}

	public Company updateCompanyName(Long id, Company company) {
		return companyRepository.updateById(id,company);
	}

	public void deleteCompany(Long id) {
		companyRepository.deleteById(id);
	}
}
